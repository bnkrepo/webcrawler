﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNK.WC.Common.SearchQuery
{
    /// <summary>   Bitfield of flags for specifying SearchFileType. </summary>
    [Flags]
    public enum SearchFileType
    {
        None,
        Unknown,
        // Documents
        PDF,
        DOC, 
        EXL,
        TXT,
        HTML,
        //Video
        MP4,
        DIVX,
        AVI,
        MKV,
        //Other
        ISO,
        //Music
        MP3,
        AAV,
        WAV,
        //Torrent
        TORRENT,
        //ebook
        EPUB
    }
}
