﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BNK.WC.Common.SearchQuery
{
    public class SearchQueryData
    {
        public string Query { get; set; }
        public SearchFileType FileType { get; set; }
        
        public string FormatedQuery 
        { 
            get 
            {
                if(FileType == SearchFileType.None)
                    return Query;
                else
                    return "FileType:" + FileType.ToString() + " + " + Query;
            }         
        }
    }
}
