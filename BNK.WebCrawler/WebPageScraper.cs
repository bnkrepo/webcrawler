﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace BNK.WebCrawler
{
    public class WebPageScraper
    {
        public WebPageScraper()
        {

        }

        public List<string> ExtractLinks(string strPageLink)
        {
            List<string> links = new List<string>();
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = hw.Load(strPageLink);
            var baseUrl = new Uri(strPageLink);
            /*
            var linksOnPage = from link in doc.DocumentNode.Descendants()
                              where link.Name == "a" 
                              where string.IsNullOrEmpty(link.Attributes["href"]) == false
                              Let text = link.InnerText.Trim()
                              Let url = link.Attributes("href").Value
                              Where url.IndexOf(linkContains, StringComparison.OrdinalIgnoreCase) >= 0 _
                              AndAlso uri.TryCreate(url, UriKind.Absolute, uri)
            Dim Uris As New List(Of Link)()
            For Each link In linksOnPage
                Uris.Add(New Link(New Uri(link.url, UriKind.Absolute), link.text))
            Next
                //*/
            Uri uri = null;

            //*
            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
            {
                Uri.TryCreate(link.Attributes["href"].Value, UriKind.Absolute, out uri);
                //var url = new Uri(new Uri(baseUrl).GetLeftPart(UriPartial.Path), link.Attributes["href"].Value);
                var temp = link.GetAttributeValue("href", string.Empty);
                links.Add(temp);
            }
            //*/
            return links;
        }
    }
}
