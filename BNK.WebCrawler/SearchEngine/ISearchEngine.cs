﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BNK.WC.Common;

namespace BNK.WebCrawler
{
    public interface ISearchEngine
    {
        List<string> GetSearchResults(string title, string language, int year);
    }
}
