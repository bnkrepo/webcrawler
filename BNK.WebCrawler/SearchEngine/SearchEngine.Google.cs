﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BNK.WC.Common.Containers;
using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;

namespace BNK.WebCrawler
{
    public class GoogleSearchEngine : ISearchEngine
    {
        //
        const string _searchEngineId    = "";
        const string _apiKey            = "";
        // 
        public List<string> GetSearchResults(string title, string language, int year)
        {
            List<string> links = new List<string>();

            SearchInput input = new SearchInput {Title = title, Language = language, ReleaseYear = year};
            
            // Online web search
            var webLinks = GetWebLinks(input);

            if (webLinks != null)
                links.AddRange(webLinks);

            // Torrents search
            var torrentsLinks = GetTorrentLinks(input);

            if (torrentsLinks != null)
                links.AddRange(torrentsLinks);

            // Site Specific Search
            var siteLinks = GetSpecificVideoSiteLinks(input);

            if (siteLinks != null)
                links.AddRange(siteLinks);

            // remove safe sites
            string safeWords = (string)Sources.Default["SafeWords"];
            char[] seperator = new char[1] { ',' };
            List<string> safeTokens = safeWords.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries).ToList();

            string safeSites = (string)Sources.Default["SafeHosts"];
            List<string> safeTokens1 = safeSites.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries).ToList();
            safeTokens.AddRange(safeTokens1);

            bool bFound = false;

            var filteredLinks = new List<string>();

            foreach (var link in links)
            {
                bFound = false;
                foreach (var safeWord in safeTokens)
                {
                    if (link.Contains(safeWord) == true)
                    {
                        Debug.WriteLine(">> " + link + " -> " + safeWord);

                        bFound = true;
                        break;
                    }
                }

                if (bFound == false)
                    filteredLinks.Add(link);
            }

            return filteredLinks;
        }

        public List<string> GetWebLinks(SearchInput searchInput)
        {             
            try
            {
                CustomsearchService customSearchService = new CustomsearchService(new Google.Apis.Services.BaseClientService.Initializer() { ApiKey = _apiKey });
                string strQuery = string.IsNullOrEmpty(searchInput.SiteToSearchIn) ?
                                                        searchInput.GetShortSearchPhrase() :
                                                        searchInput.GetSiteSearchPhrase();

                Debug.WriteLine(">> " + strQuery + " <<");

                CseResource.ListRequest listRequest = customSearchService.Cse.List(strQuery);
                
                listRequest.Cx = _searchEngineId;
                Search search = listRequest.Execute();

                return search.Items.ToList().Select(item => item.Link).ToList();
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public List<string> GetTorrentLinks(SearchInput input)
        {
            List<string> links = new List<string>();

            string torrentSites = (string)Sources.Default["TorrentSite"];
            char[] seperator = new char[1] { ',' };
            string [] tokens = torrentSites.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries);

            return GetSpecificSiteLinks(input, tokens);
        }

        public List<string> GetSpecificVideoSiteLinks(SearchInput input)
        {
            string videoSites = (string)Sources.Default["VideoSite"];
            char[] seperator = new char[1] { ',' };
            string[] tokens = videoSites.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries);

            return GetSpecificSiteLinks(input, tokens);
        }

        private List<string> GetSpecificSiteLinks(SearchInput input, string [] sites)
        {
            List<string> links = new List<string>();

            foreach (string site in sites)
            {
                input.SiteToSearchIn = site;
                var list = GetWebLinks(input);

                if (list != null)
                    links.AddRange(list);
            }

            return links;
        }
    }
}
