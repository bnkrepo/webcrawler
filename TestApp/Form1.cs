﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BNK;
using BNK.WebCrawler;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) == false)
            {
                textBox2.Text = "";

                GoogleSearchEngine google = new GoogleSearchEngine();
                List<string> links = google.GetSearchResults(textBox1.Text, "hindi", 2015);

                if (links != null)
                {
                    links.ForEach(link =>
                        {                          
                            textBox2.Text += link;

                            if (link.Contains("youtube.com") == true)
                                textBox2.Text += " " + BNK.YoutubeHelper.GetVideoDuration(link.Remove(0, 32)).ToString();

                            textBox2.Text += Environment.NewLine;
                        }
                    );
                }
            }
        }
    }
}
